# Install dependencies
FROM ubuntu:16.04
# Clean and update
RUN apt-get clean && apt-get update
RUN apt-get -y install curl  
RUN apt-get -y install wget  
RUN apt-get -y install apt-utils
RUN apt-get -y install nano
RUN apt-get autoremove -y
RUN apt-get update

# Node app
FROM node:10.17
# set working directory
RUN mkdir /usr/src/program
WORKDIR /usr/src/program

# add `/usr/src/program/node_modules/.bin` to $PATH
ENV PATH /usr/src/program/node_modules/.bin:$PATH

# install and cache app dependencies
ADD package.json /tmp/program/
COPY . /usr/src/program/ 

RUN npm config set maxsockets 5 && npm config set progress false
RUN npm install 
RUN npm run build

RUN npm install -g serve
# Install pm2
RUN npm install -g pm2

COPY . /usr/src/program/ 
EXPOSE 5000
# start app
CMD ["pm2", "start", "process.yml", "--no-daemon"]