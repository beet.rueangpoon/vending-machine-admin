import React from 'react';
import { Provider } from 'react-redux'
import { Route } from 'react-router-dom'
import store from './store'
import './App.css';
import './App.scss';

import HomePage from './component/HomePage'
import LoginPage from './component/LoginPage'
import LogoutPage from './component/LogoutPage'

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/login" component={LoginPage} />
        <Route exact path="/logout" component={LogoutPage} />
      </Provider>
    </div>
  );
}

export default App;
