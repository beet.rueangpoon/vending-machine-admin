import { LOGIN_SUCCESS, LOGOUT } from '../actions/types';

export function loginSuccess(payload) {
    return {
        type: LOGIN_SUCCESS,
        payload: payload
    };
}

export function logoutSuccess() {
    return {
        type: LOGOUT,
    };
}
