import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom'
import { Layout, Menu } from 'antd';
import {
    ProfileOutlined,
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    ShoppingCartOutlined,
    ShoppingOutlined,
    UserOutlined,
    LogoutOutlined,
    ShopOutlined,
    BankOutlined,
} from '@ant-design/icons';

import StockBalance from '../../container/StockBalance'
import Orders from '../../container/Orders'
import Product from '../../container/Product'
import VendingMachine from '../../container/VendingMachine'
import Inventory from '../../container/Inventory'
import User from '../../container/User'

import logo from '../../resource/images/logo.png'
import logoWhite from '../../resource/images/logo_square.png'
import './styles.scss'
const { Header, Sider, Content } = Layout;

function HomePage(props) {
    const [collapsed, setCollapsed] = useState(true)
    const [selectedKeys, setSelectedKeys] = useState('1')
    const [selectedIndex, setSelectedIndex] = useState(0)
    const componentList = [
        { key: '1', name: 'stock-balance', label: 'Stock Balance', component: <StockBalance {...props} />, icon: <ProfileOutlined /> },
        { key: '2', name: 'oprders', label: 'Orders', component: <Orders {...props} />, icon: <ShoppingCartOutlined /> },
        { key: '3', name: 'product', label: 'Product', component: <Product {...props} />, icon: <ShoppingOutlined /> },
        { key: '4', name: 'vending-machine', label: 'Vending Machine', component: <VendingMachine {...props} />, icon: <ShopOutlined /> },
        { key: '5', name: 'inventory', label: 'Inventory', component: <Inventory {...props} />, icon: <BankOutlined /> },
        { key: '6', name: 'user', label: 'User', component: <User {...props} />, icon: <UserOutlined /> },
        { key: '7', name: 'logout', label: 'Logout', component: null, icon: <LogoutOutlined /> },
    ]

    const onClickLeftMemu = (item, index) => {
        if (item.name === 'logout') {
            props.history.push('/logout')
        } else {
            setSelectedKeys(item.key)
            setSelectedIndex(index)
        }
    }

    if (props.isAuthenticated === false || props.isAuthenticated === "false") {
        return <Redirect from="/" to={'/login'} />
    }
    return (
        <Layout className="administrator-layout-page-wrapper">
            <Sider trigger={null} collapsible collapsed={collapsed}>
                {collapsed ?
                    <Link to={'/'}><img className="logo-collapsed" src={logoWhite} /></Link>
                    :
                    <Link to={'/'}><img className="logo" src={logo} /></Link>
                }
                <Menu theme="dark" mode="inline" selectedKeys={[selectedKeys]}>
                    {componentList.map((item, index) => {
                        return (
                            <Menu.Item
                                key={item.key}
                                icon={item.icon}
                                onClick={() => onClickLeftMemu(item, index)}
                            >
                                {item.label}
                            </Menu.Item>
                        )
                    })}
                </Menu>
            </Sider>
            <Layout className="site-layout">
                <Header className="site-layout-header-background">
                    <div className="icon-wrapper">
                        {collapsed ?
                            <MenuUnfoldOutlined onClick={() => setCollapsed(!collapsed)} style={{ fontSize: 18 }} />
                            :
                            <MenuFoldOutlined onClick={() => setCollapsed(!collapsed)} style={{ fontSize: 18 }} />
                        }
                    </div>
                    <h1>{"Vending Machine (Administrator)"}</h1>
                </Header>
                <Content
                    className="site-layout-background"
                    style={{
                        margin: '24px 16px',
                        padding: 24,
                        minHeight: 280,
                    }}
                >
                    {componentList[selectedIndex].component}
                </Content>
            </Layout>
        </Layout>
    );
}

const mapStateToProps = (state) => ({
    isAuthenticated: state.login.isAuthenticated
})

export default connect(mapStateToProps)(HomePage);