import React from 'react';
import { Col, Row, Button, Popconfirm } from 'antd'; 
// import defaultSquareSrc from '../../resource/default/300x300-square.png'
import {
    DeleteOutlined,
    UploadOutlined,
} from '@ant-design/icons';

import './style.scss'

class ImageUpload extends React.Component {

    onClickButtonUpload(event) {
        let fileSelect = Array.from(this.refs.imageFile.files)
        let dataSource = this.props.dataSource.map(item => Object.assign({}, item))
        if (fileSelect && fileSelect.length > 0) {
            let temp = { file: fileSelect[0] }
            let reader = new FileReader();
            reader.readAsDataURL(fileSelect[0]);
            reader.onload = (e) => {
                temp['view'] = e.target.result
                dataSource.push(temp);
                this.props.updateDataSource(dataSource)
            };
            event.target.value = null;
        }
    }

    onClickButtonDelete(index) {
        let dataSource = this.props.dataSource.filter((data, i) => i != index)
        this.props.updateDataSource(dataSource)
    }

    render() {
        const { label, limit, dataSource, previewImage, className, disabled } = this.props
        return (
            <div className={className}>
                <Row
                    type="flex"
                    className='image-upload-container'
                >
                    {previewImage ?
                        <div className='preview-image-wrapper' >
                            <img
                                id="target"
                                style={{ width: '100%', height: '100%' }}
                                src={previewImage}
                                // onError={(image) => image.target.src = defaultSquareSrc}
                            />
                        </div>
                        : null
                    }
                    {dataSource.length < limit ?
                        <div
                            className='upload-button-wrapper'
                            onClick={() => disabled ? null : this.refs.imageFile.click()}
                            style={{
                                opacity: disabled ? 0.5 : 1,
                                cursor: disabled ? 'not-allowed' : 'pointer'
                            }}
                        >
                            <input
                                multiple={false} // false only ! if not function check storage is incorrect.
                                id="imageFile"
                                type="file"
                                ref="imageFile"
                                onChange={(event) => this.onClickButtonUpload(event)}
                                style={{ display: 'none' }}
                            />
                            <UploadOutlined style={{ fontSize: '50px' }} />
                            <p>{label}</p>
                        </div>
                        : null
                    }
                    {dataSource.map((item, index) => {
                        return (
                            <div className='view-image-wrapper' >
                                <img
                                    id="target"
                                    src={item.view}
                                    // onError={(image) => image.target.src = defaultSquareSrc}
                                />
                                <div
                                    className='delete-button-wrapper'
                                    style={{ display: disabled ? 'none' : 'block' }}
                                >
                                    <Popconfirm title="Sure to delete?" onConfirm={() => this.onClickButtonDelete(index)}>
                                        <Button style={{ width: '100%', height: '25px' }}><DeleteOutlined /></Button>
                                    </Popconfirm>
                                </div>
                            </div>
                        )
                    })}
                </Row>

            </div>
        )
    }
}

export default ImageUpload;