import React, { useState } from 'react';
import { connect } from 'react-redux';
import { loginSuccess } from '../../actions/login';
import { Redirect } from 'react-router-dom'
import { Input, notification, Modal, Button, Spin } from 'antd';
import { post } from '../../utils';

import logo from '../../resource/images/logo.png'
import './styles.scss'

const openNotificationWithIcon = (type, message, description) => {
    notification[type]({
        message: message,
        description: description,
    });
};

function LoginPage(props) {
    const [loading, setLoading] = useState(false);
    const [username, setEmail] = useState('');
    const [password, setPassword] = useState('');


    const loginUser = () => {
        if (!username || username === '') {
            openNotificationWithIcon('warning', "Required failed", 'Please input "Username"')
            return
        }
        if (!password || password === '') {
            openNotificationWithIcon('warning', "Required failed", 'Please input "Password"')
            return
        }
        let url = '/public/user/login'
        let data = {
            username: username,
            password: password
        }
        setLoading(true)
        post(url, data)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad responses from server");
                }
                if (response.success) {
                    props.loginSuccess(response.user)
                    openNotificationWithIcon('success', "Authentication passed", 'Login success')
                } else {
                    if (response.error.code === "SELECT_USER_WHERE_USERNAME_NOT_FOUND") {
                        openNotificationWithIcon('error', "Authentication failed", 'Not found username')
                    }
                    if (response.error.code === "PASSWORD_IS_INCORRECT") {
                        openNotificationWithIcon('error', "Authentication failed", 'Password is incorrect')
                    }
                }
                setLoading(false)
            })
            .catch(err => {
                console.log(err.message)
                setLoading(false)
            })
    }

    const handleOk = () => {
        loginUser()
    };

    const handleCancel = () => {
        props.history.push('/')
    };

    if (props.isAuthenticated === true || props.isAuthenticated === "true") {
        return <Redirect from="/login" to={'/'} />
    }
    return (
        <div>
            <Modal
                title={null}
                visible={true}
                onOk={handleOk}
                onCancel={handleCancel}
                footer={null}
                closable={false}
                wrapClassName={"login-page-wrapper"}
            >
                <Spin spinning={loading} tip="Loading...">
                    <div className="login-page-container" >
                        <div className='login-image-wrapper'>
                            <img className='login-image' src={logo} />
                        </div>
                        <div className="login-title-wrapper">
                            <p className="login-title" >{"Log in to Vending Machine"}</p>
                        </div>
                        <div className="login-input-wrapper">
                            <div className="login-input-item-wrapper">
                                <Input
                                    value={username}
                                    placeholder={'Username'}
                                    onChange={(event) => setEmail(event.target.value)}
                                />
                            </div>
                            <div >
                                <Input
                                    value={password}
                                    type={"password"}
                                    placeholder={'Password'}
                                    onChange={(event) => setPassword(event.target.value)}
                                />
                            </div>
                        </div>
                        <div className='login-button-wrapper' >
                            <Button className='login-button' onClick={() => loginUser()}>{"Login"}</Button>
                        </div>
                    </div>
                </Spin>
            </Modal>
        </div>
    );
}

const mapStateToProps = (state) => ({
    isAuthenticated: state.login.isAuthenticated
})

const mapDispatchToProps = (dispatch) => ({
    loginSuccess: (payload) => dispatch(loginSuccess(payload))
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);