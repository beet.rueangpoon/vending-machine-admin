import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { logoutSuccess } from '../../actions/login';
import { Redirect } from 'react-router-dom'  

import './styles.scss'

function LogoutPage(props) {

    useEffect(() => {
        props.logoutSuccess()
    }, [])
 
    if (props.isAuthenticated === false || props.isAuthenticated === 'false') {
        return <Redirect from="/login" to={'/'} />
    }
    return null;
}

const mapStateToProps = (state) => ({
    isAuthenticated: state.login.isAuthenticated, 
})

const mapDispatchToProps = (dispatch) => ({
    logoutSuccess: () => dispatch(logoutSuccess())
})

export default connect(mapStateToProps, mapDispatchToProps)(LogoutPage);