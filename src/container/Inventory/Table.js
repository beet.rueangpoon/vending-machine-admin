import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { get } from '../../utils';
import { Table } from 'antd';

import './styles.scss'

function InventoryTable(props) {
    const [dataSource, setDataSource] = useState([]);
    const [loading, setLoading] = useState(false)

    const columns = [
        {
            title: 'Inventory ID',
            dataIndex: 'id',
            key: 'id',
            width: '15%',
            align: 'center',
            render: (text, record) => (record.id).toString().padStart(10, "0")
        },
        {
            title: 'Vending Machine',
            dataIndex: 'vending_machine_name',
            key: 'vending_machine_name',
            width: '20%', 
        },
        {
            title: 'Product',
            dataIndex: 'product_name',
            key: 'product_name',
        },
        {
            title: 'Quantity',
            dataIndex: 'quantity',
            key: 'quantity',
            width: '15%',
            align: 'center',
        },
        {
            title: 'DateTime',
            dataIndex: 'create_date_time',
            key: 'create_date_time',
            width: '15%',
            align: 'center',
            render: (text, record) => record.create_date_time ? moment(record.create_date_time).format("YYYY-MM-DD HH:mm:ss") : null
        },
    ];

    useEffect(() => {
        setLoading(true)
        let url = '/api/inventory'
        get(url)
            .then(responses => {
                if (responses.status >= 400) {
                    throw new Error("Bad responses from server");
                }
                setDataSource(responses)
                setLoading(false)
            })
            .catch(err => {
                console.log(err.message)
                if(err.message === 'Unauthorized'){
                    props.history.push('/logout')
                } 
                setLoading(false)
            })
    }, []);

    return (
        <div className="inventory-table-wrapper">
            <Table
                className="inventory-table"
                bordered
                loading={loading}
                dataSource={dataSource}
                columns={columns}
                onRow={(record, rowIndex) => {
                    return {
                        onClick: (event) => props.onClickRow(record),
                    };
                }}
            />
        </div>
    );
}

export default InventoryTable;
