import React, { useState, useEffect } from 'react';
import { Row, Col, Form, Input, Select, Card, Button, Spin, notification } from 'antd';

import { get, put, post } from '../../utils';
import InventoryTable from './Table';

import './styles.scss'

const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;

const formItemLayout = {
    labelCol: {
        xs: { span: 8 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 16 },
        sm: { span: 16 },
    },
};

const openNotificationWithIcon = (type, message, description) => {
    notification[type]({
        message: message,
        description: description,
    });
};

function Inventory(props) {
    const [visable, setVisable] = useState(false)
    const [editId, setEditId] = useState(null)
    const [loading, setLoading] = useState(false)
    const [vendingMachineList, setVendingMachineList] = useState([])
    const [productList, setProductList] = useState([])

    const [vendingMachineId, setVendingMachineId] = useState(null)
    const [productId, setProductId] = useState(null)
    const [quantity, setQuantity] = useState(1)

    const getVendingMachineList = () => {
        let url = '/api/vending/machine'
        get(url)
            .then(responses => {
                if (responses.status >= 400) {
                    throw new Error("Bad responses from server");
                }
                setVendingMachineList(responses)
            })
            .catch(err => {
                console.log(err.message)
            })
    }

    const getProductList = () => {
        let url = '/api/product?deleted=0'
        get(url)
            .then(responses => {
                if (responses.status >= 400) {
                    throw new Error("Bad responses from server");
                }
                setProductList(responses)
            })
            .catch(err => {
                console.log(err.message)
            })
    }

    useEffect(() => {
        getVendingMachineList()
        getProductList()
    }, []);

    const onclickCreateButton = () => {
        setVisable(true)
    }

    const onclickEditButton = (record) => {
        setEditId(record.id)
        setVisable(true)
        setLoading(true)
        let url = '/api/inventory/' + record.id
        get(url)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                if (response) {
                    setVendingMachineId(response.vending_machine_id)
                    setProductId(response.product_id)
                    setQuantity(response.quantity)
                }
                setLoading(false)
            })
            .catch(err => {
                console.log(err.message)
                setLoading(false)
            })
    }

    const onclickSaveButton = () => {
        if (!vendingMachineId) {
            openNotificationWithIcon('warning', "Required failed", 'Please select "Vending Machine"')
            return
        }
        if (!productId) {
            openNotificationWithIcon('warning', "Required failed", 'Please select "Equipment"')
            return
        }
        if (!quantity || quantity === '') {
            openNotificationWithIcon('warning', "Required failed", 'Please input "Quantity"')
            return
        }
        setLoading(true)
        let url = '/api/inventory'
        let update = post
        let data = {
            vending_machine_id: vendingMachineId,
            product_id: productId,
            quantity: quantity,
        }
        if (editId) {
            url = '/api/inventory/' + editId
            update = put
        }
        update(url, data)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                setEditId(null)
                setVisable(false)
                setLoading(false)
                clearState()
            })
            .catch(err => {
                console.log(err.message)
                setLoading(false)
            })
    }

    const onclickCancelButton = () => {
        setEditId(null)
        setVisable(false)
        clearState()
    }

    const clearState = () => {
        setVendingMachineId(null)
        setProductId(null)
        setQuantity(1)

    }

    const getHeaderTitle = () => {
        if (editId) {
            return 'Edit - Inventory'
        }
        if (visable) {
            return 'Create - Inventory'
        }
        return 'Inventory'
    }

    return (
        <div className="inventory-page-wrapper">
            <div className="header-wrapper">
                <div className="title-wrapper">
                    <h2>{getHeaderTitle()}</h2>
                </div>
                <div className="action-button-wrapper">
                    {!visable ? <Button type="primary" onClick={() => onclickCreateButton()}>{'Create'}</Button> : null}
                    {visable ? <Button type="primary" onClick={() => onclickSaveButton()}>{'Save'}</Button> : null}
                    {visable ? <Button type="danger " onClick={() => onclickCancelButton()}>{'Cancel'}</Button> : null}
                </div>
            </div>

            {!visable ?
                <InventoryTable history={props.history} onClickRow={(record) => onclickEditButton(record)} />
                :
                <div className="inventory-main-wrapper">
                    <Spin tip="Loading..." spinning={loading}>
                        <Card className="inventory-main-card" >
                            <Form>
                                <Row gutter={24}>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                                        <FormItem {...formItemLayout} label={'Vending Machine'} required={true}>
                                            <Select
                                                value={vendingMachineId}
                                                onChange={(value) => setVendingMachineId(value)}
                                            >
                                                {vendingMachineList.map(item => <Option key={item.id} value={item.id}>{item.name}</Option>)}
                                            </Select>
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                                        <FormItem {...formItemLayout} label={'Product'} required={true}>
                                            <Select
                                                value={productId}
                                                onChange={(value) => setProductId(value)}
                                            >
                                                {productList.map(item => <Option key={item.id} value={item.id}>{item.name}</Option>)}
                                            </Select>
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                                        <FormItem {...formItemLayout} label={'Quantity'} required={true}>
                                            <Input
                                                type={'number'}
                                                value={quantity}
                                                onChange={(event) => setQuantity(event.target.value)}
                                            />
                                        </FormItem>
                                    </Col>
                                </Row>
                            </Form>
                        </Card>
                    </Spin>
                </div>
            }
        </div>
    );
}

export default Inventory;
