import React from 'react';
import { connect } from 'react-redux';
import OrdersTable from './Table';


import './styles.scss'

function Orders(props) {
    const getHeaderTitle = () => {
        return 'Orders'
    }
    return (
        <div className="orders-page-wrapper">
            <div className="header-wrapper">
                <div className="title-wrapper">
                    <h2>{getHeaderTitle()}</h2>
                </div>
            </div>
            <OrdersTable history={props.history} userId={props.userId} />
        </div>
    );
}

const mapStateToProps = (state) => ({
    userId: state.login.id
})

export default connect(mapStateToProps)(Orders);