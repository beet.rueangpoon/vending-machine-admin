import React, { useEffect, useState } from 'react';
import { get } from '../../utils';
import { Table } from 'antd';

import './styles.scss'

function ProductTable(props) {
    const [dataSource, setDataSource] = useState([]);
    const [loading, setLoading] = useState(false)

    const columns = [
        {
            title: 'Product ID',
            dataIndex: 'id',
            key: 'id',
            width: '20%',
            align: 'center',
            render: (text, record) => (record.id).toString().padStart(10, "0")
        },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Price (THB)',
            dataIndex: 'price',
            key: 'price',
        },
        {
            title: 'Deleted',
            dataIndex: 'deleted',
            key: 'deleted',
            width: '20%',
            align: 'center',
            render: (text, record) => record.deleted ? <span style={{ color: "red" }}>{"Yes"}</span> : <span style={{ color: "green" }}>{"No"}</span>
        },
    ];

    useEffect(() => {
        setLoading(true)
        let url = '/api/product'
        get(url)
            .then(responses => {
                if (responses.status >= 400) {
                    throw new Error("Bad responses from server");
                }
                setDataSource(responses)
                setLoading(false)
            })
            .catch(err => {
                console.log(err.message)
                if(err.message === 'Unauthorized'){
                    props.history.push('/logout')
                } 
                setLoading(false)
            })
    }, []);

    return (
        <div className="product-table-wrapper">
            <Table
                className="product-table"
                bordered
                loading={loading}
                dataSource={dataSource}
                columns={columns}
                onRow={(record, rowIndex) => {
                    return {
                        onClick: (event) => props.onClickRow(record),
                    };
                }}
            />
        </div>
    );
}

export default ProductTable;
