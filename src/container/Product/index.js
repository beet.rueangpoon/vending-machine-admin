import React, { useState } from 'react';
import { Row, Col, Form, Input, Select, Card, Button, Spin, notification } from 'antd';

import { get, put, post, ppost } from '../../utils';
import ProductTable from './Table';
import ImageUpload from '../../component/ImageUpload';
import './styles.scss'

const UPLOAD_URL = `${process.env.REACT_APP_API_PROTOCOL}://${process.env.REACT_APP_API_HOSTNAME}:${process.env.REACT_APP_API_PORT}/uploads/`

const Option = Select.Option;
const FormItem = Form.Item;

const formItemLayout = {
    labelCol: {
        xs: { span: 8 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 16 },
        sm: { span: 16 },
    },
};

const openNotificationWithIcon = (type, message, description) => {
    notification[type]({
        message: message,
        description: description,
    });
};

function Product(props) {
    const [visable, setVisable] = useState(false)
    const [editId, setEditId] = useState(null)
    const [loading, setLoading] = useState(false)
    const deletedList = [
        { value: 0, label: 'No' },
        { value: 1, label: 'Yes' }
    ]

    const [name, setName] = useState('')
    const [imageList, setImageList] = useState([])
    const [price, setPrice] = useState(0)
    const [deleted, setDeleted] = useState(0)

    const onclickCreateButton = () => {
        setVisable(true)
    }

    const onclickEditButton = (record) => {
        setEditId(record.id)
        setVisable(true)
        setLoading(true)
        let url = '/api/product/' + record.id
        get(url)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                if (response) {
                    let imageList = []
                    if (response.image) {
                        imageList = [{
                            view: UPLOAD_URL + response.image,
                            file: null,
                            name: response.image
                        }]
                    }
                    setName(response.name)
                    setImageList(imageList)
                    setPrice(response.price)
                    setDeleted(response.deleted)
                }
                setLoading(false)
            })
            .catch(err => {
                console.log(err.message)
                setLoading(false)
            })
    }


    const uploadImageToServer = async (formData) => {
        console.log('formData', formData)
        let url = '/api/upload/file'
        return await ppost(url, formData)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                return response
            })
            .catch(err => {
                console.log(err.message)
                return []
            })
    }

    const onclickSaveButton = async () => {
        if (!name || name === '') {
            openNotificationWithIcon('warning', "Required failed", 'Please input "Name"')
            return
        }
        setLoading(true)
        let url = '/api/product'
        let update = post
        let data = {
            name: name,
            price: price,
            deleted: deleted,
        }

        let uploadImageList = imageList.filter(item => item.file)
        let uploadDataList = imageList.filter(item => !item.file)
        if (uploadImageList.length) {
            let formData = new FormData();
            formData.append("product", uploadImageList[0].file)
            let fileNameList = await uploadImageToServer(formData)
            console.log('fileNameList', fileNameList)
            if (fileNameList.length) {
                data.image = fileNameList[0]
            }
        } else {
            if (uploadDataList.length) {
                data.image = imageList[0].name
            }
        }


        if (editId) {
            url = '/api/product/' + editId
            update = put
        }
        update(url, data)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                setEditId(null)
                setVisable(false)
                setLoading(false)
                clearState()
            })
            .catch(err => {
                console.log(err.message)
                setLoading(false)
            })
    }

    const onclickCancelButton = () => {
        setEditId(null)
        setVisable(false)
        clearState()
    }

    const clearState = () => {
        setName('')
        setImageList([])
        setPrice(0)
        setDeleted(0)
    }

    const getHeaderTitle = () => {
        if (editId) {
            return 'Edit - Product'
        }
        if (visable) {
            return 'Create - Product'
        }
        return 'Product'
    }

    return (
        <div className="product-page-wrapper">
            <div className="header-wrapper">
                <div className="title-wrapper">
                    <h2>{getHeaderTitle()}</h2>
                </div>
                <div className="action-button-wrapper">
                    {!visable ? <Button type="primary" onClick={() => onclickCreateButton()}>{'Create'}</Button> : null}
                    {visable ? <Button type="primary" onClick={() => onclickSaveButton()}>{'Save'}</Button> : null}
                    {visable ? <Button type="danger " onClick={() => onclickCancelButton()}>{'Cancel'}</Button> : null}
                </div>
            </div>

            {!visable ?
                <ProductTable history={props.history} onClickRow={(record) => onclickEditButton(record)} />
                :
                <div className="product-main-wrapper">
                    <Spin tip="Loading..." spinning={loading}>
                        <Card className="product-main-card" >
                            <Form>
                                <Row gutter={24}>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                                        <FormItem {...formItemLayout} label={'Name'}>
                                            <Input
                                                value={name}
                                                onChange={(event) => setName(event.target.value)}
                                            />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                                        <FormItem {...formItemLayout} label={'Image'}>
                                            <ImageUpload
                                                label={'Upload image'}
                                                limit={1}
                                                dataSource={imageList}
                                                updateDataSource={(dataSource) => setImageList(dataSource)}
                                            />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                                        <FormItem {...formItemLayout} label={'Price'}>
                                            <Input
                                                type={'number'}
                                                value={price}
                                                onChange={(event) => setPrice(event.target.value)}
                                            />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                                        <FormItem {...formItemLayout} label={'Deleted'} required={true}>
                                            <Select
                                                value={deleted}
                                                onChange={(value) => setDeleted(value)}
                                            >
                                                {deletedList.map(item => <Option key={item.value} value={item.value}>{item.label}</Option>)}
                                            </Select>
                                        </FormItem>
                                    </Col>
                                </Row>
                            </Form>
                        </Card>
                    </Spin>
                </div>
            }
        </div>
    );
}

export default Product;
