import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { get } from '../../utils';
import { Table } from 'antd';

import './styles.scss'

function StockBalanceTable(props) {
    const [dataSource, setDataSource] = useState([]);
    const [loading, setLoading] = useState(false)

    const columns = [
        {
            title: 'Product ID',
            dataIndex: 'id',
            key: 'id',
            width: '20%',
            align: 'center',
            render: (text, record) => (record.id).toString().padStart(10, "0")
        },
        {
            title: 'Product',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Quantity',
            dataIndex: 'quantity',
            key: 'quantity',
            width: '20%',
            align: 'center',
            render: (text, record) => record.quantity < 10 ? <span style={{ color: "red" }}>{text}</span> : <span style={{ color: "green" }}>{text}</span>
        }
    ];



    useEffect(() => {
        if (props.vendingMachineId) {
            setLoading(true)
            let url = '/api/stock/balance?vending_machine=' + props.vendingMachineId
            get(url)
                .then(responses => {
                    if (responses.status >= 400) {
                        throw new Error("Bad responses from server");
                    }
                    setDataSource(responses)
                    setLoading(false)
                })
                .catch(err => {
                    console.log(err.message)
                    if(err.message === 'Unauthorized'){
                        props.history.push('/logout')
                    } 
                    setLoading(false) 
                })
        }
    }, [props.vendingMachineId]);

    return (
        <div className="stock-balance-table-wrapper">
            <Table
                className="stock-balance-table"
                bordered
                loading={loading}
                dataSource={dataSource}
                columns={columns}
            />
        </div>
    );
}

export default StockBalanceTable;
