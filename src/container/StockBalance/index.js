import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Form, Select } from 'antd';

import { get } from '../../utils';
import StockBalanceTable from './Table';


import './styles.scss'

const FormItem = Form.Item;
const Option = Select.Option;


const formItemLayout = {
    labelCol: {
        xs: { span: 8 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 16 },
        sm: { span: 16 },
    },
};

function StockBalance(props) {
    const [vendingMachineList, setVendingMachineList] = useState([])
    const [vendingMachineId, setVendingMachineId] = useState(null)

    const getVendingMachineList = () => {
        let url = '/api/vending/machine'
        get(url)
            .then(responses => {
                if (responses.status >= 400) {
                    throw new Error("Bad responses from server");
                }
                setVendingMachineList(responses)
                if (responses.length) {
                    setVendingMachineId(responses[0].id)
                }
            })
            .catch(err => {
                console.log(err.message)
                if (err.message === 'Unauthorized') {
                    props.history.push('/logout')
                }
            })
    }

    // const getGeolocation = ()=>  {
    //     if ("geolocation" in navigator) { 
    //       navigator.geolocation.getCurrentPosition(
    //         (position) => {
    //           console.log("Position : ", position); 
    //         },
    //         (error) => {
    //           console.error(error.message);
    //         }
    //       );
    //     } else {
    //       console.log("Not Available");
    //     }
    //   }

    useEffect(() => {
        // getGeolocation()
        getVendingMachineList()
    }, []);

    const getHeaderTitle = () => {
        return 'Stock Balance'
    }

    return (
        <div className="stock-balance-page-wrapper">
            <div className="header-wrapper">
                <div className="title-wrapper">
                    <h2>{getHeaderTitle()}</h2>
                </div>
                <div className="action-select-wrapper">
                    <Row gutter={24}>
                        <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                            <FormItem {...formItemLayout} label={'Vending Machine'}>
                                <Select
                                    value={vendingMachineId}
                                    onChange={(value) => setVendingMachineId(value)}
                                >
                                    {vendingMachineList.map(item => <Option key={item.id} value={item.id}>{item.name}</Option>)}
                                </Select>
                            </FormItem>
                        </Col>
                    </Row>
                </div>
            </div>

            <StockBalanceTable history={props.history} userId={props.userId} vendingMachineId={vendingMachineId} />
        </div>
    );
}

const mapStateToProps = (state) => ({
    userId: state.login.id,
    role: state.login.role,
    firstname: state.login.firstname,
    lastname: state.login.lastname,
})

export default connect(mapStateToProps)(StockBalance);