import React, { useEffect, useState } from 'react';
import { get } from '../../utils';
import { Table } from 'antd';

import './styles.scss'

function UserTable(props) {
    const [dataSource, setDataSource] = useState([]);
    const [loading, setLoading] = useState(false)

    const columns = [
        {
            title: 'User ID',
            dataIndex: 'id',
            key: 'id',
            width: '15%',
            align: 'center',
            render: (text, record) => (record.id).toString().padStart(10, "0")
        }, 
        {
            title: 'Firstname',
            dataIndex: 'firstname',
            key: 'firstname',
        },
        {
            title: 'Lastname',
            dataIndex: 'lastname',
            key: 'lastname',
        },
        {
            title: 'Username',
            dataIndex: 'username',
            key: 'username',
        },
        {
            title: 'Deleted',
            dataIndex: 'deleted',
            key: 'deleted',
            width: '15%',
            align: 'center',
            render: (text, record) => record.deleted ? <span style={{ color: "red" }}>{"Yes"}</span> : <span style={{ color: "green" }}>{"No"}</span>
        },
    ];

    useEffect(() => {
        setLoading(true)
        let url = '/api/user'
        get(url)
            .then(responses => {
                if (responses.status >= 400) {
                    throw new Error("Bad responses from server");
                }
                setDataSource(responses)
                setLoading(false)
            })
            .catch(err => {
                console.log(err.message)
                if(err.message === 'Unauthorized'){
                    props.history.push('/logout')
                } 
                setLoading(false)
            })
    }, []);

    return (
        <div className="user-table-wrapper">
            <Table
                className="user-table"
                bordered
                loading={loading}
                dataSource={dataSource}
                columns={columns}
                onRow={(record, rowIndex) => {
                    return {
                        onClick: (event) => props.onClickRow(record),
                    };
                }}
            />
        </div>
    );
}

export default UserTable;
