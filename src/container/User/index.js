import React, { useState, useEffect } from 'react';
import { Row, Col, Form, Input, Select, Card, Button, Spin, notification } from 'antd';

import { get, put, post } from '../../utils';
import UserTable from './Table';

import './styles.scss'

const FormItem = Form.Item;
const Option = Select.Option;

const formItemLayout = {
    labelCol: {
        xs: { span: 8 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 16 },
        sm: { span: 16 },
    },
};

const openNotificationWithIcon = (type, message, description) => {
    notification[type]({
        message: message,
        description: description,
    });
};

function User(props) {
    const [visable, setVisable] = useState(false)
    const [editId, setEditId] = useState(null)
    const [loading, setLoading] = useState(false) 
    const deletedList = [
        { value: 0, label: 'No' },
        { value: 1, label: 'Yes' }
    ]
 
    const [firstname, setFirstname] = useState('')
    const [lastname, setLastname] = useState('')
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('') 
    const [deleted, setDeleted] = useState(0)


    const onclickCreateButton = () => {
        setVisable(true)
    }

    const onclickEditButton = (record) => {
        setEditId(record.id)
        setVisable(true)
        setLoading(true)
        let url = '/api/user/' + record.id
        get(url)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                if (response) { 
                    setUsername(response.username) 
                    setFirstname(response.firstname)
                    setLastname(response.lastname)
                    setDeleted(response.deleted)
                }
                setLoading(false)
            })
            .catch(err => {
                console.log(err.message)
                setLoading(false)
            })
    }

    const onclickSaveButton = () => {
        if (!firstname || firstname === '') {
            openNotificationWithIcon('warning', "Required failed", 'Please input "Firstname"')
            return
        }
        if (!lastname || lastname === '') {
            openNotificationWithIcon('warning', "Required failed", 'Please input "Lastname"')
            return
        }
        if (!username || username === '') {
            openNotificationWithIcon('warning', "Required failed", 'Please input "Username"')
            return
        }
        if ((!password || password === '') && !editId) {
            openNotificationWithIcon('warning', "Required failed", 'Please input "password"')
            return
        } 

        setLoading(true)
        let url = '/api/user'
        let update = post
        let data = { 
            firstname: firstname,
            lastname: lastname,
            username: username,
            password: password,
            deleted: deleted
        }
        if (editId) {
            url = '/api/user/' + editId
            update = put
        }
        update(url, data)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                setEditId(null)
                setVisable(false)
                setLoading(false)
                clearState()
            })
            .catch(err => {
                console.log(err.message)
                setLoading(false)
            })
    }

    const onclickCancelButton = () => {
        setEditId(null)
        setVisable(false)
        clearState()
    }

    const clearState = () => { 
        setUsername('')
        setPassword('')
        setFirstname('')
        setLastname('')
        setDeleted(0)
    }

    const getHeaderTitle = () => {
        if (editId) {
            return 'Edit - User'
        }
        if (visable) {
            return 'Create - User'
        }
        return 'User'
    }

    return (
        <div className="user-page-wrapper">
            <div className="header-wrapper">
                <div className="title-wrapper">
                    <h2>{getHeaderTitle()}</h2>
                </div>
                <div className="action-button-wrapper">
                    {!visable ? <Button type="primary" onClick={() => onclickCreateButton()}>{'Create'}</Button> : null}
                    {visable ? <Button type="primary" onClick={() => onclickSaveButton()}>{'Save'}</Button> : null}
                    {visable ? <Button type="danger " onClick={() => onclickCancelButton()}>{'Cancel'}</Button> : null}
                </div>
            </div>

            {!visable ?
                <UserTable history={props.history} onClickRow={(record) => onclickEditButton(record)} />
                :
                <div className="user-main-wrapper">
                    <Spin tip="Loading..." spinning={loading}>
                        <Card className="user-main-card" >
                            <Form>
                                <Row gutter={24}>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                                        <FormItem {...formItemLayout} label={'Firstname'} required={true}>
                                            <Input
                                                value={firstname}
                                                onChange={(event) => setFirstname(event.target.value)}
                                            />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                                        <FormItem {...formItemLayout} label={'Lastname'} required={true}>
                                            <Input
                                                value={lastname}
                                                onChange={(event) => setLastname(event.target.value)}
                                            />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                                        <FormItem {...formItemLayout} label={'Username'} required={true}>
                                            <Input
                                                value={username}
                                                type={'username'}
                                                onChange={(event) => setUsername(event.target.value)}
                                            />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                                        <FormItem {...formItemLayout} label={'Password'} required={true}>
                                            <Input
                                                value={password}
                                                type={'password'}
                                                onChange={(event) => setPassword(event.target.value)}
                                            />
                                        </FormItem>
                                    </Col> 
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                                        <FormItem {...formItemLayout} label={'Deleted'} required={true}>
                                            <Select
                                                value={deleted}
                                                onChange={(value) => setDeleted(value)}
                                            >
                                                {deletedList.map(item => <Option key={item.value} value={item.value}>{item.label}</Option>)}
                                            </Select>
                                        </FormItem>
                                    </Col>
                                </Row>
                            </Form>
                        </Card>
                    </Spin>
                </div>
            }
        </div>
    );
}

export default User;
