import React, { useEffect, useState } from 'react';
import { get } from '../../utils';
import { Table } from 'antd';

import './styles.scss'

function VendingMachineTable(props) {
    const [dataSource, setDataSource] = useState([]);
    const [loading, setLoading] = useState(false)

    const columns = [
        {
            title: 'Vending Machine ID',
            dataIndex: 'id',
            key: 'id',
            width: '20%',
            align: 'center',
            render: (text, record) => (record.id).toString().padStart(10, "0")
        },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Serial Number',
            dataIndex: 'serial_number',
            key: 'serial_number',
        },
    ];

    useEffect(() => {
        setLoading(true)
        let url = '/api/vending/machine'
        get(url)
            .then(responses => {
                if (responses.status >= 400) {
                    throw new Error("Bad responses from server");
                }
                setDataSource(responses)
                setLoading(false)
            })
            .catch(err => {
                console.log(err.message)
                if (err.message === 'Unauthorized') {
                    props.history.push('/logout')
                }
                setLoading(false)
            })
    }, []);

    return (
        <div className="vending-machine-table-wrapper">
            <Table
                className="vending-machine-table"
                bordered
                loading={loading}
                dataSource={dataSource}
                columns={columns}
                onRow={(record, rowIndex) => {
                    return {
                        onClick: (event) => props.onClickRow(record),
                    };
                }}
            />
        </div>
    );
}

export default VendingMachineTable;
