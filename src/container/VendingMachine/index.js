import React, { useState, useEffect } from 'react';
import { Row, Col, Form, Input, Select, Card, Button, Spin, notification } from 'antd';

import { get, put, post } from '../../utils';
import VendingMachineTable from './Table';

import './styles.scss'

const FormItem = Form.Item;
const Option = Select.Option;
const { TextArea } = Input;

const formItemLayout = {
    labelCol: {
        xs: { span: 8 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 16 },
        sm: { span: 16 },
    },
};

const openNotificationWithIcon = (type, message, description) => {
    notification[type]({
        message: message,
        description: description,
    });
};

function VendingMachine(props) {
    const [visable, setVisable] = useState(false)
    const [editId, setEditId] = useState(null)
    const [loading, setLoading] = useState(false)

    const [name, setName] = useState('')
    const [serialNumber, setSerialNumber] = useState('')
    const [locationLatitude, setLocationLatitude] = useState(null)
    const [locationLongitude, setLocationLongitude] = useState(null)
    const [updateDateTime, setUpdateDateTime] = useState(null)

    const onclickCreateButton = () => {
        setVisable(true)
    }

    const onclickEditButton = (record) => {
        setEditId(record.id)
        setVisable(true)
        setLoading(true)
        let url = '/api/vending/machine/' + record.id
        get(url)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                if (response) {
                    setName(response.name)
                    setSerialNumber(response.serial_number)
                    setLocationLatitude(response.location_latitude)
                    setLocationLongitude(response.location_longitude)
                    setUpdateDateTime(response.update_date_time)
                }
                setLoading(false)
            })
            .catch(err => {
                console.log(err.message)
                setLoading(false)
            })
    }

    const onclickSaveButton = () => {
        if (!name || name === '') {
            openNotificationWithIcon('warning', "Required failed", 'Please input "name"')
            return
        }
        setLoading(true)
        let url = '/api/vending/machine'
        let update = post
        let data = {
            name: name,
        }
        if (editId) {
            url = '/api/vending/machine/' + editId
            update = put
        }
        update(url, data)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                setEditId(null)
                setVisable(false)
                setLoading(false)
                clearState()
            })
            .catch(err => {
                console.log(err.message)
                setLoading(false)
            })
    }

    const onclickCancelButton = () => {
        setEditId(null)
        setVisable(false)
        clearState()
    }

    const clearState = () => {
        setName('')
        setSerialNumber(null)
        setLocationLatitude(null)
        setLocationLongitude(null)
        setUpdateDateTime(null)
    }

    const getHeaderTitle = () => {
        if (editId) {
            return 'Edit - Vending Machine'
        }
        if (visable) {
            return 'Create - Vending Machine'
        }
        return 'Vending Machine'
    }

    return (
        <div className="vending-machine-page-wrapper">
            <div className="header-wrapper">
                <div className="title-wrapper">
                    <h2>{getHeaderTitle()}</h2>
                </div>
                <div className="action-button-wrapper">
                    {!visable ? <Button type="primary" onClick={() => onclickCreateButton()}>{'Create'}</Button> : null}
                    {visable ? <Button type="primary" onClick={() => onclickSaveButton()}>{'Save'}</Button> : null}
                    {visable ? <Button type="danger " onClick={() => onclickCancelButton()}>{'Cancel'}</Button> : null}
                </div>
            </div>

            {!visable ?
                <VendingMachineTable history={props.history} onClickRow={(record) => onclickEditButton(record)} />
                :
                <div className="vending-machine-main-wrapper">
                    <Spin tip="Loading..." spinning={loading}>
                        <Card className="vending-machine-main-card" >
                            <Form>
                                <Row gutter={24}>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                                        <FormItem {...formItemLayout} label={'Name'} required={true}>
                                            <Input
                                                value={name}
                                                onChange={(event) => setName(event.target.value)}
                                            />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                                        <FormItem {...formItemLayout} label={'Code'} >
                                            <Input
                                                value={serialNumber}
                                                disabled={true}
                                            />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                                        <FormItem {...formItemLayout} label={'location Latitute'}>
                                            <Input
                                                value={locationLatitude}
                                                disabled={true}
                                            />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                                        <FormItem {...formItemLayout} label={'location Longitute'} >
                                            <Input
                                                value={locationLongitude}
                                                disabled={true}
                                            />
                                        </FormItem>
                                    </Col>
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} >
                                        <FormItem {...formItemLayout} label={'DateTime'}>
                                            <Input
                                                value={updateDateTime}
                                                disabled={true}
                                            />
                                        </FormItem>
                                    </Col>

                                </Row>
                            </Form>
                        </Card>
                    </Spin>
                </div>
            }
        </div>
    );
}

export default VendingMachine;
