const initialState = {
    id: localStorage.getItem('id') ?  parseInt(localStorage.getItem('id')) : '',
    firstname: localStorage.getItem('firstname') ? localStorage.getItem('firstname') : '',
    lastname: localStorage.getItem('lastname') ? localStorage.getItem('lastname') : '',
    username: localStorage.getItem('username') ? localStorage.getItem('username') : '',  
    isAuthenticated: localStorage.getItem('isAuthenticated') ? localStorage.getItem('isAuthenticated') : false,
};


const login = (state = initialState, action) => {
    switch (action.type) {
        case 'LOGIN_SUCCESS': {
            localStorage.setItem('isAuthenticated', true)
            localStorage.setItem('id', action.payload.id)
            localStorage.setItem('firstname', action.payload.firstname)
            localStorage.setItem('lastname', action.payload.lastname)
            localStorage.setItem('username', action.payload.username) 
            localStorage.setItem('token', action.payload.token) 
            return Object.assign({}, state, {
                isAuthenticated: true,
                id: action.payload.id,
                firstname: action.payload.firstname,
                lastname: action.payload.lastname,
                username: action.payload.username,  
            })
        }
        case 'LOGOUT': {
            localStorage.setItem('isAuthenticated', false)
            localStorage.setItem('id', '')
            localStorage.setItem('firstname', '')
            localStorage.setItem('lastname', '')
            localStorage.setItem('username', '') 
            localStorage.setItem('token', '')
            return Object.assign({}, state, {
                isAuthenticated: false,
                id: '',
                firstname: '',
                lastname: '',
                username: '',  
            })
        }
        default:
            return state;
    }
}

export default login;