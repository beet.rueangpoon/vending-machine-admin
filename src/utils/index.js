const PROTOCOL = process.env.REACT_APP_API_PROTOCOL
const HOSTNAME = process.env.REACT_APP_API_HOSTNAME
const PORT = process.env.REACT_APP_API_PORT

console.log('process.env : ', process.env)

function call(url, method, data) { 
    const serverUrl = `${PROTOCOL}://${HOSTNAME}:${PORT}${url}`
    console.log(`${method} Url : `, serverUrl) 
    return fetch(serverUrl, {
        method: method,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
            'x-access-token': localStorage.getItem('token'),
        },
        body: JSON.stringify(data)
    })
        .then(parseJSON)
        .then(checkHttpStatus)
        .catch(error => {
            console.log("error at utils.index.js line 21")
            console.log(error)
            if (typeof error.response === 'undefined') {
                error.response = {
                    status: 408,
                    message: 'Cannot connect to the server'
                }
            }
            throw error
        })
}

function checkHttpStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response.body
    } else {
        var error = new Error(response.statusText)
        error.response = response.body
        throw error
    }
}

function parseJSON(response) {
    return response.json()
        .then(function (body) {
            return {
                status: response.status,
                statusText: response.statusText,
                body: body
            }
        })
        .catch(function (e) {
            return response;
        })
}

export function get(url) {
    return call(url, 'GET')
}

export function post(url, data) {
    return call(url, 'POST', data)
}

export function put(url, data) {
    return call(url, 'PUT', data)
}

export function patch(url, data) {
    return call(url, 'PATCH', data)
}

export function del(url, data) {
    return call(url, 'DELETE', data)
}



export function ppost(url, data) {
    return ccall(url, 'POST', data)
}

export function ccall(url, method, data) { 
    const serverUrl = `${PROTOCOL}://${HOSTNAME}:${PORT}${url}`
    console.log(`${method} Url : `, serverUrl) 
    return fetch(serverUrl, {
        method: method,
        headers: {
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
            'x-access-token': localStorage.getItem('token'),
        },
        body: data
    })
        .then(parseJSON)
        .then(checkHttpStatus)
        .catch(error => {
            // No response from the server
            console.log("error at utils.index.js line 107")
            console.log(error)
            if (typeof error.response === 'undefined') {
                error.response = {
                    status: 408,
                    message: 'Cannot connect to the server'
                }
            }
            throw error
        })
}
